﻿using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace HealthPro
{
    public partial class Home : Form
    {
        ///<summary>
        /// 存放插件的集合
        ///</summary>
        private Hashtable hashPluginClass = new Hashtable();
        /// <summary>
        /// 插件参数
        /// </summary>
        private string[] logUs = null;

        public Home()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            ///< 插件装载
            LoadAllPlugs();

            ///< 弄个假的
            logUs = new string[2];
            logUs[0] = "username";
            logUs[1] = "password";

            ///< 将插件转换为实体对象【同时设置好参数】
            PluginClass.loadObjectModule(hashPluginClass, logUs);

            ///< 实例化默认功能
            PluginClass.showForm(PluginClass.getObject(PluginClass.FUNCTION_MODULE.USER_HOS_EXCELL), this.UI_panel, true);
            this.bone_radioButton.ForeColor = Color.Red;
        }

        ///<summary>
        /// 载入所有插件
        ///</summary>
        private void LoadAllPlugs()
        {
            String pluginPath;
#if (!DEBUG)
            pluginPath = Application.StartupPath + @"\plugins";
#else
            pluginPath = @"D:\MEME\Excells\HealthPro\HosBoneExcell\bin\Debug";
#endif
            ///< 获取插件目录(plugins)下所有文件
            string[] files = Directory.GetFiles(pluginPath);
            foreach (string file in files)
            {
                if (file.ToUpper().EndsWith(".DLL"))
                {
                    try
                    {
                        ///< 载入dll
                        Assembly ab = Assembly.LoadFrom(file);
                        Type[] types = ab.GetTypes();
                        foreach (Type t in types)
                        {
                            ///< 如果某些类实现了预定义的IMsg.IMsgPlug接口，则认为该类适配与主程序(是主程序的插件)
                            if (t.GetInterface("IMsgPlug") != null)
                            {
                                hashPluginClass.Add(t.FullName, ab.CreateInstance(t.FullName));
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// group边框颜色去除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_groupBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(this.BackColor);
        }

        /// <summary>
        /// 骨科录入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bone_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            PluginClass.showForm(PluginClass.getObject(PluginClass.FUNCTION_MODULE.USER_HOS_EXCELL), this.UI_panel, this.bone_radioButton.Checked);
            this.bone_radioButton.ForeColor = this.bone_radioButton.Checked ? Color.Red : Color.Black;
        }

        /*
        /// <summary>
        /// 可拖动处理
        /// </summary>
        private Point mousePoint = new Point();
    
        private void Home_MouseDown(object sender, MouseEventArgs e)
        {
            this.mousePoint.X = e.X;
            this.mousePoint.Y = e.Y;
        }

        private void Home_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Top = Control.MousePosition.Y - mousePoint.Y;
                this.Left = Control.MousePosition.X - mousePoint.X;
            }
        }*/
    }
}
