﻿namespace HealthPro
{
    partial class Home
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.UI_panel = new System.Windows.Forms.Panel();
            this.MoredoweblinkLabel = new System.Windows.Forms.LinkLabel();
            this.menu_groupBox = new System.Windows.Forms.GroupBox();
            this.other_radioButton = new System.Windows.Forms.RadioButton();
            this.bone_radioButton = new System.Windows.Forms.RadioButton();
            this.menu_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // UI_panel
            // 
            this.UI_panel.BackColor = System.Drawing.Color.YellowGreen;
            this.UI_panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.UI_panel.Location = new System.Drawing.Point(184, 71);
            this.UI_panel.Margin = new System.Windows.Forms.Padding(0);
            this.UI_panel.Name = "UI_panel";
            this.UI_panel.Size = new System.Drawing.Size(1004, 604);
            this.UI_panel.TabIndex = 24;
            // 
            // MoredoweblinkLabel
            // 
            this.MoredoweblinkLabel.ActiveLinkColor = System.Drawing.Color.Black;
            this.MoredoweblinkLabel.AutoSize = true;
            this.MoredoweblinkLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Italic);
            this.MoredoweblinkLabel.Location = new System.Drawing.Point(2, 9);
            this.MoredoweblinkLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.MoredoweblinkLabel.Name = "MoredoweblinkLabel";
            this.MoredoweblinkLabel.Size = new System.Drawing.Size(180, 46);
            this.MoredoweblinkLabel.TabIndex = 19;
            this.MoredoweblinkLabel.TabStop = true;
            this.MoredoweblinkLabel.Text = "医院官网";
            // 
            // menu_groupBox
            // 
            this.menu_groupBox.Controls.Add(this.other_radioButton);
            this.menu_groupBox.Controls.Add(this.bone_radioButton);
            this.menu_groupBox.Location = new System.Drawing.Point(4, 58);
            this.menu_groupBox.Name = "menu_groupBox";
            this.menu_groupBox.Size = new System.Drawing.Size(173, 617);
            this.menu_groupBox.TabIndex = 27;
            this.menu_groupBox.TabStop = false;
            this.menu_groupBox.Paint += new System.Windows.Forms.PaintEventHandler(this.menu_groupBox_Paint);
            // 
            // other_radioButton
            // 
            this.other_radioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.other_radioButton.AutoSize = true;
            this.other_radioButton.Font = new System.Drawing.Font("宋体", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.other_radioButton.Location = new System.Drawing.Point(6, 76);
            this.other_radioButton.Name = "other_radioButton";
            this.other_radioButton.Size = new System.Drawing.Size(153, 43);
            this.other_radioButton.TabIndex = 1;
            this.other_radioButton.TabStop = true;
            this.other_radioButton.Text = "敬请期待";
            this.other_radioButton.UseVisualStyleBackColor = true;
            // 
            // bone_radioButton
            // 
            this.bone_radioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.bone_radioButton.AutoSize = true;
            this.bone_radioButton.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.bone_radioButton.Font = new System.Drawing.Font("宋体", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bone_radioButton.Location = new System.Drawing.Point(6, 12);
            this.bone_radioButton.Name = "bone_radioButton";
            this.bone_radioButton.Size = new System.Drawing.Size(153, 43);
            this.bone_radioButton.TabIndex = 0;
            this.bone_radioButton.TabStop = true;
            this.bone_radioButton.Text = "骨科录入";
            this.bone_radioButton.UseVisualStyleBackColor = true;
            this.bone_radioButton.CheckedChanged += new System.EventHandler(this.bone_radioButton_CheckedChanged);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(1197, 687);
            this.Controls.Add(this.menu_groupBox);
            this.Controls.Add(this.MoredoweblinkLabel);
            this.Controls.Add(this.UI_panel);
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hos信息录入平台";
            this.Load += new System.EventHandler(this.Home_Load);
            this.menu_groupBox.ResumeLayout(false);
            this.menu_groupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel UI_panel;
        private System.Windows.Forms.LinkLabel MoredoweblinkLabel;
        private System.Windows.Forms.GroupBox menu_groupBox;
        private System.Windows.Forms.RadioButton bone_radioButton;
        private System.Windows.Forms.RadioButton other_radioButton;
    }
}

