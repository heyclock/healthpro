﻿using System;
using System.Collections;
using System.Reflection;
using System.Windows.Forms;

namespace HealthPro
{
    class PluginClass
    {
        private static Type t = null;
        private static MethodInfo setParamFunc = null;
        private static MethodInfo getFormFunc = null;

        private static Hashtable objectHash = new Hashtable();
        public enum FUNCTION_MODULE
        {
            USER_HOS_EXCELL
        };

        /// <summary>
        /// 装载模块类到Hash对象表
        /// </summary>
        /// <param name="hashPluginClass"></param>
        public static void loadObjectModule(Hashtable hashPluginClass, string[] logUs)
        {
            objectHash.Add(FUNCTION_MODULE.USER_HOS_EXCELL, hashPluginClass[System.Configuration.ConfigurationManager.AppSettings["HosBoneExcell"]]);

            ///< 同时传递参数到插件【没有必要每次调用都设置】
            setParams(objectHash[FUNCTION_MODULE.USER_HOS_EXCELL], logUs);
        }

        /// <summary>
        /// 通过类型获取对象实体
        /// </summary>
        /// <param name="funcModule"></param>
        /// <returns></returns>
        public static object getObject(FUNCTION_MODULE funcModule)
        {
            return objectHash[funcModule];
        }

        /// <summary>
        /// 启用插件前首先需要设置参数、最好提前设置，免去每次调用插件都设置【当然插件可以参数可以无效或者不使用】
        /// </summary>
        /// <param name="selObj"></param>
        /// <param name="logUs"></param>
        public static void setParams(object selObj, string[] logUs)
        {
            t = selObj.GetType();
            setParamFunc = t.GetMethod("setParam");

            ///< 把参数传进去
            setParamFunc.Invoke(selObj, new object[] { logUs });
        }

        /// <summary>
        /// 内部函数，通过对象获取Form实体
        /// </summary>
        /// <param name="selObj"></param>
        /// <returns></returns>
        private static Form getParams(object selObj)
        {
            t = selObj.GetType();
            getFormFunc = t.GetMethod("getForm");
            return (Form)getFormFunc.Invoke(selObj, null);
        }

        /// <summary>
        /// 显示/隐藏窗体【如果插件是一个窗体的话】
        /// </summary>
        /// <param name="selObj"></param>
        /// <param name="itsPanel"></param>
        public static void showForm(object selObj, Panel itsPanel, bool bShow)
        {
            Form form = getParams(selObj);
            form.TopLevel = false;
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            form.Dock = DockStyle.Fill;
            form.Parent = itsPanel;
            if (bShow)
            {
                form.Show();
            }
            else
            {
                form.Hide();
            }
            form.BringToFront();
        }
    }
}
