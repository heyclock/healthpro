﻿using NPOI.HSSF.UserModel;
using System;

namespace HosBoneExcell
{
    class HssModelBean
    {
        public bool state { get; set; }
        public String message { get; set; }
        public HSSFSheet hSSFSheet { get; set; }
        public HSSFWorkbook hssfworkbookDown { get; set; }
    }
}
