﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using RMB_Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace HosBoneExcell 
{
    public class HosBoneClass : HosBoneForm, FormInterface.IMsgPlug
    {
        private System.Windows.Forms.Button bonneRecord_button;
        private Label hosid_label;
        private TextBox hosid_textBox;
        private TextBox name_textBox;
        private Label name_label;
        private Label sex_label;
        private TextBox age_textBox;
        private Label age_label;
        private TextBox height_textBox;
        private Label height_label;
        private TextBox weight_textBox;
        private Label weight_label;
        private TextBox dignose_textBox;
        private Label diagnosis_label;
        private TextBox osteotomylenght_textBox;
        private Label osteotomy_label;
        private TextBox quadricepsfr_textBox;
        private Label straighteningptss_label;
        private TextBox msts_textBox;
        private Label straighteningptnormal_label;
        private Label straighteningpt_label;
        private TextBox straighteningptss_textBox;
        private Label quadricepsfr_label;
        private TextBox straighteningptnormal_textBox;
        private Label MSTS_label;
        private Label bucklingpfv_label;
        private TextBox bucklingpfvnormal_textBox;
        private Label bucklingpfvnormal_label;
        private TextBox bucklingpfvss_textBox;
        private Label bucklingpfvss_label;
        private Label normalizedpm_label;
        private TextBox normalizedpmnormal_textBox;
        private Label normalizedpmnormal_label;
        private TextBox normalizedpmss_textBox;
        private Label normalizedpmss_label;
        private Label normalizedppf_label;
        private TextBox normalizedppfnormal_ltextBox;
        private Label normalizedppfnormal_label;
        private TextBox normalizedppfss_textBox;
        private Label normalizedppfss_label;
        private TextBox directory_textBox;
        private TextBox outexcellname_textBox;
        private Label flex_label;
        private Button derectory_button;
        private Button outexcellfile_button;
        private string userPass = null;
        private ComboBox six_comboBox;

        #region IMsgPlug 成员

        /// <summary>
        /// 登录信息
        /// </summary>
        private string userName = null;

        public void setParam(string[] parameters)
        {
            userName = parameters[0];
            userPass = parameters[1];
        }

        public object getForm()
        {
            return (Object)this;
        }

        #endregion

        private void InitializeComponent()
        {
            this.bonneRecord_button = new System.Windows.Forms.Button();
            this.hosid_label = new System.Windows.Forms.Label();
            this.hosid_textBox = new System.Windows.Forms.TextBox();
            this.name_textBox = new System.Windows.Forms.TextBox();
            this.name_label = new System.Windows.Forms.Label();
            this.sex_label = new System.Windows.Forms.Label();
            this.age_textBox = new System.Windows.Forms.TextBox();
            this.age_label = new System.Windows.Forms.Label();
            this.height_textBox = new System.Windows.Forms.TextBox();
            this.height_label = new System.Windows.Forms.Label();
            this.weight_textBox = new System.Windows.Forms.TextBox();
            this.weight_label = new System.Windows.Forms.Label();
            this.dignose_textBox = new System.Windows.Forms.TextBox();
            this.diagnosis_label = new System.Windows.Forms.Label();
            this.osteotomylenght_textBox = new System.Windows.Forms.TextBox();
            this.osteotomy_label = new System.Windows.Forms.Label();
            this.quadricepsfr_textBox = new System.Windows.Forms.TextBox();
            this.straighteningptss_label = new System.Windows.Forms.Label();
            this.msts_textBox = new System.Windows.Forms.TextBox();
            this.straighteningptnormal_label = new System.Windows.Forms.Label();
            this.straighteningpt_label = new System.Windows.Forms.Label();
            this.straighteningptss_textBox = new System.Windows.Forms.TextBox();
            this.quadricepsfr_label = new System.Windows.Forms.Label();
            this.straighteningptnormal_textBox = new System.Windows.Forms.TextBox();
            this.MSTS_label = new System.Windows.Forms.Label();
            this.bucklingpfv_label = new System.Windows.Forms.Label();
            this.bucklingpfvnormal_textBox = new System.Windows.Forms.TextBox();
            this.bucklingpfvnormal_label = new System.Windows.Forms.Label();
            this.bucklingpfvss_textBox = new System.Windows.Forms.TextBox();
            this.bucklingpfvss_label = new System.Windows.Forms.Label();
            this.normalizedpm_label = new System.Windows.Forms.Label();
            this.normalizedpmnormal_textBox = new System.Windows.Forms.TextBox();
            this.normalizedpmnormal_label = new System.Windows.Forms.Label();
            this.normalizedpmss_textBox = new System.Windows.Forms.TextBox();
            this.normalizedpmss_label = new System.Windows.Forms.Label();
            this.normalizedppf_label = new System.Windows.Forms.Label();
            this.normalizedppfnormal_ltextBox = new System.Windows.Forms.TextBox();
            this.normalizedppfnormal_label = new System.Windows.Forms.Label();
            this.normalizedppfss_textBox = new System.Windows.Forms.TextBox();
            this.normalizedppfss_label = new System.Windows.Forms.Label();
            this.directory_textBox = new System.Windows.Forms.TextBox();
            this.outexcellname_textBox = new System.Windows.Forms.TextBox();
            this.flex_label = new System.Windows.Forms.Label();
            this.derectory_button = new System.Windows.Forms.Button();
            this.outexcellfile_button = new System.Windows.Forms.Button();
            this.six_comboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // bonneRecord_button
            // 
            this.bonneRecord_button.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bonneRecord_button.Location = new System.Drawing.Point(812, 560);
            this.bonneRecord_button.Name = "bonneRecord_button";
            this.bonneRecord_button.Size = new System.Drawing.Size(180, 40);
            this.bonneRecord_button.TabIndex = 1;
            this.bonneRecord_button.Text = "录入";
            this.bonneRecord_button.UseVisualStyleBackColor = true;
            this.bonneRecord_button.Click += new System.EventHandler(this.bonneRecord_button_Click);
            // 
            // hosid_label
            // 
            this.hosid_label.AutoSize = true;
            this.hosid_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hosid_label.Location = new System.Drawing.Point(12, 18);
            this.hosid_label.Name = "hosid_label";
            this.hosid_label.Size = new System.Drawing.Size(82, 24);
            this.hosid_label.TabIndex = 2;
            this.hosid_label.Text = "住院号";
            // 
            // hosid_textBox
            // 
            this.hosid_textBox.Location = new System.Drawing.Point(111, 18);
            this.hosid_textBox.Name = "hosid_textBox";
            this.hosid_textBox.Size = new System.Drawing.Size(224, 28);
            this.hosid_textBox.TabIndex = 3;
            this.hosid_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.hosid_textBox_KeyPress);
            // 
            // name_textBox
            // 
            this.name_textBox.Location = new System.Drawing.Point(111, 77);
            this.name_textBox.Name = "name_textBox";
            this.name_textBox.Size = new System.Drawing.Size(224, 28);
            this.name_textBox.TabIndex = 5;
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.name_label.Location = new System.Drawing.Point(12, 76);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(82, 24);
            this.name_label.TabIndex = 4;
            this.name_label.Text = "姓  名";
            // 
            // sex_label
            // 
            this.sex_label.AutoSize = true;
            this.sex_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sex_label.Location = new System.Drawing.Point(12, 136);
            this.sex_label.Name = "sex_label";
            this.sex_label.Size = new System.Drawing.Size(82, 24);
            this.sex_label.TabIndex = 6;
            this.sex_label.Text = "性  别";
            // 
            // age_textBox
            // 
            this.age_textBox.Location = new System.Drawing.Point(111, 202);
            this.age_textBox.Name = "age_textBox";
            this.age_textBox.Size = new System.Drawing.Size(224, 28);
            this.age_textBox.TabIndex = 9;
            this.age_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.age_textBox_KeyPress);
            // 
            // age_label
            // 
            this.age_label.AutoSize = true;
            this.age_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.age_label.Location = new System.Drawing.Point(12, 202);
            this.age_label.Name = "age_label";
            this.age_label.Size = new System.Drawing.Size(82, 24);
            this.age_label.TabIndex = 8;
            this.age_label.Text = "年  龄";
            // 
            // height_textBox
            // 
            this.height_textBox.Location = new System.Drawing.Point(111, 269);
            this.height_textBox.Name = "height_textBox";
            this.height_textBox.Size = new System.Drawing.Size(224, 28);
            this.height_textBox.TabIndex = 11;
            this.height_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.height_textBox_KeyPress);
            // 
            // height_label
            // 
            this.height_label.AutoSize = true;
            this.height_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.height_label.Location = new System.Drawing.Point(12, 266);
            this.height_label.Name = "height_label";
            this.height_label.Size = new System.Drawing.Size(82, 24);
            this.height_label.TabIndex = 10;
            this.height_label.Text = "身  高";
            // 
            // weight_textBox
            // 
            this.weight_textBox.Location = new System.Drawing.Point(111, 332);
            this.weight_textBox.Name = "weight_textBox";
            this.weight_textBox.Size = new System.Drawing.Size(224, 28);
            this.weight_textBox.TabIndex = 13;
            this.weight_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.weight_textBox_KeyPress);
            // 
            // weight_label
            // 
            this.weight_label.AutoSize = true;
            this.weight_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.weight_label.Location = new System.Drawing.Point(12, 329);
            this.weight_label.Name = "weight_label";
            this.weight_label.Size = new System.Drawing.Size(82, 24);
            this.weight_label.TabIndex = 12;
            this.weight_label.Text = "体  重";
            // 
            // dignose_textBox
            // 
            this.dignose_textBox.Location = new System.Drawing.Point(111, 395);
            this.dignose_textBox.Name = "dignose_textBox";
            this.dignose_textBox.Size = new System.Drawing.Size(224, 28);
            this.dignose_textBox.TabIndex = 15;
            // 
            // diagnosis_label
            // 
            this.diagnosis_label.AutoSize = true;
            this.diagnosis_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.diagnosis_label.Location = new System.Drawing.Point(12, 395);
            this.diagnosis_label.Name = "diagnosis_label";
            this.diagnosis_label.Size = new System.Drawing.Size(82, 24);
            this.diagnosis_label.TabIndex = 14;
            this.diagnosis_label.Text = "诊  断";
            // 
            // osteotomylenght_textBox
            // 
            this.osteotomylenght_textBox.Location = new System.Drawing.Point(111, 456);
            this.osteotomylenght_textBox.Name = "osteotomylenght_textBox";
            this.osteotomylenght_textBox.Size = new System.Drawing.Size(224, 28);
            this.osteotomylenght_textBox.TabIndex = 17;
            this.osteotomylenght_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.osteotomylenght_textBox_KeyPress);
            // 
            // osteotomy_label
            // 
            this.osteotomy_label.AutoSize = true;
            this.osteotomy_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.osteotomy_label.Location = new System.Drawing.Point(1, 456);
            this.osteotomy_label.Name = "osteotomy_label";
            this.osteotomy_label.Size = new System.Drawing.Size(106, 24);
            this.osteotomy_label.TabIndex = 16;
            this.osteotomy_label.Text = "截骨长度";
            // 
            // quadricepsfr_textBox
            // 
            this.quadricepsfr_textBox.Location = new System.Drawing.Point(113, 515);
            this.quadricepsfr_textBox.Name = "quadricepsfr_textBox";
            this.quadricepsfr_textBox.Size = new System.Drawing.Size(222, 28);
            this.quadricepsfr_textBox.TabIndex = 19;
            // 
            // straighteningptss_label
            // 
            this.straighteningptss_label.AutoSize = true;
            this.straighteningptss_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.straighteningptss_label.Location = new System.Drawing.Point(453, 59);
            this.straighteningptss_label.Name = "straighteningptss_label";
            this.straighteningptss_label.Size = new System.Drawing.Size(82, 24);
            this.straighteningptss_label.TabIndex = 18;
            this.straighteningptss_label.Text = "手续侧";
            // 
            // msts_textBox
            // 
            this.msts_textBox.Location = new System.Drawing.Point(113, 560);
            this.msts_textBox.Name = "msts_textBox";
            this.msts_textBox.Size = new System.Drawing.Size(222, 28);
            this.msts_textBox.TabIndex = 21;
            this.msts_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.msts_textBox_KeyPress);
            // 
            // straighteningptnormal_label
            // 
            this.straighteningptnormal_label.AutoSize = true;
            this.straighteningptnormal_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.straighteningptnormal_label.Location = new System.Drawing.Point(725, 59);
            this.straighteningptnormal_label.Name = "straighteningptnormal_label";
            this.straighteningptnormal_label.Size = new System.Drawing.Size(82, 24);
            this.straighteningptnormal_label.TabIndex = 20;
            this.straighteningptnormal_label.Text = "正常侧";
            // 
            // straighteningpt_label
            // 
            this.straighteningpt_label.AutoSize = true;
            this.straighteningpt_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.straighteningpt_label.Location = new System.Drawing.Point(635, 15);
            this.straighteningpt_label.Name = "straighteningpt_label";
            this.straighteningpt_label.Size = new System.Drawing.Size(130, 24);
            this.straighteningpt_label.TabIndex = 22;
            this.straighteningpt_label.Text = "伸直峰力矩";
            // 
            // straighteningptss_textBox
            // 
            this.straighteningptss_textBox.Location = new System.Drawing.Point(540, 59);
            this.straighteningptss_textBox.Name = "straighteningptss_textBox";
            this.straighteningptss_textBox.Size = new System.Drawing.Size(178, 28);
            this.straighteningptss_textBox.TabIndex = 39;
            this.straighteningptss_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.straighteningptss_textBox_KeyPress);
            // 
            // quadricepsfr_label
            // 
            this.quadricepsfr_label.AutoSize = true;
            this.quadricepsfr_label.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.quadricepsfr_label.Location = new System.Drawing.Point(-3, 515);
            this.quadricepsfr_label.Name = "quadricepsfr_label";
            this.quadricepsfr_label.Size = new System.Drawing.Size(116, 18);
            this.quadricepsfr_label.TabIndex = 38;
            this.quadricepsfr_label.Text = "股四头肌切除";
            // 
            // straighteningptnormal_textBox
            // 
            this.straighteningptnormal_textBox.Location = new System.Drawing.Point(812, 55);
            this.straighteningptnormal_textBox.Name = "straighteningptnormal_textBox";
            this.straighteningptnormal_textBox.Size = new System.Drawing.Size(180, 28);
            this.straighteningptnormal_textBox.TabIndex = 41;
            this.straighteningptnormal_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.straighteningptnormal_textBox_KeyPress);
            // 
            // MSTS_label
            // 
            this.MSTS_label.AutoSize = true;
            this.MSTS_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MSTS_label.Location = new System.Drawing.Point(1, 560);
            this.MSTS_label.Name = "MSTS_label";
            this.MSTS_label.Size = new System.Drawing.Size(106, 24);
            this.MSTS_label.TabIndex = 40;
            this.MSTS_label.Text = "MSTS评分";
            // 
            // bucklingpfv_label
            // 
            this.bucklingpfv_label.AutoSize = true;
            this.bucklingpfv_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bucklingpfv_label.Location = new System.Drawing.Point(635, 118);
            this.bucklingpfv_label.Name = "bucklingpfv_label";
            this.bucklingpfv_label.Size = new System.Drawing.Size(130, 24);
            this.bucklingpfv_label.TabIndex = 46;
            this.bucklingpfv_label.Text = "屈曲峰力值";
            // 
            // bucklingpfvnormal_textBox
            // 
            this.bucklingpfvnormal_textBox.Location = new System.Drawing.Point(813, 162);
            this.bucklingpfvnormal_textBox.Name = "bucklingpfvnormal_textBox";
            this.bucklingpfvnormal_textBox.Size = new System.Drawing.Size(179, 28);
            this.bucklingpfvnormal_textBox.TabIndex = 45;
            this.bucklingpfvnormal_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.bucklingpfvnormal_textBox_KeyPress);
            // 
            // bucklingpfvnormal_label
            // 
            this.bucklingpfvnormal_label.AutoSize = true;
            this.bucklingpfvnormal_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bucklingpfvnormal_label.Location = new System.Drawing.Point(725, 162);
            this.bucklingpfvnormal_label.Name = "bucklingpfvnormal_label";
            this.bucklingpfvnormal_label.Size = new System.Drawing.Size(82, 24);
            this.bucklingpfvnormal_label.TabIndex = 44;
            this.bucklingpfvnormal_label.Text = "正常侧";
            // 
            // bucklingpfvss_textBox
            // 
            this.bucklingpfvss_textBox.Location = new System.Drawing.Point(541, 162);
            this.bucklingpfvss_textBox.Name = "bucklingpfvss_textBox";
            this.bucklingpfvss_textBox.Size = new System.Drawing.Size(178, 28);
            this.bucklingpfvss_textBox.TabIndex = 43;
            this.bucklingpfvss_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.bucklingpfvss_textBox_KeyPress);
            // 
            // bucklingpfvss_label
            // 
            this.bucklingpfvss_label.AutoSize = true;
            this.bucklingpfvss_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bucklingpfvss_label.Location = new System.Drawing.Point(453, 162);
            this.bucklingpfvss_label.Name = "bucklingpfvss_label";
            this.bucklingpfvss_label.Size = new System.Drawing.Size(82, 24);
            this.bucklingpfvss_label.TabIndex = 42;
            this.bucklingpfvss_label.Text = "手续侧";
            // 
            // normalizedpm_label
            // 
            this.normalizedpm_label.AutoSize = true;
            this.normalizedpm_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.normalizedpm_label.Location = new System.Drawing.Point(635, 217);
            this.normalizedpm_label.Name = "normalizedpm_label";
            this.normalizedpm_label.Size = new System.Drawing.Size(178, 24);
            this.normalizedpm_label.TabIndex = 51;
            this.normalizedpm_label.Text = "归一后伸直力矩";
            // 
            // normalizedpmnormal_textBox
            // 
            this.normalizedpmnormal_textBox.Enabled = false;
            this.normalizedpmnormal_textBox.Location = new System.Drawing.Point(813, 266);
            this.normalizedpmnormal_textBox.Name = "normalizedpmnormal_textBox";
            this.normalizedpmnormal_textBox.Size = new System.Drawing.Size(179, 28);
            this.normalizedpmnormal_textBox.TabIndex = 50;
            this.normalizedpmnormal_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.normalizedpmnormal_textBox_KeyPress);
            // 
            // normalizedpmnormal_label
            // 
            this.normalizedpmnormal_label.AutoSize = true;
            this.normalizedpmnormal_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.normalizedpmnormal_label.Location = new System.Drawing.Point(725, 266);
            this.normalizedpmnormal_label.Name = "normalizedpmnormal_label";
            this.normalizedpmnormal_label.Size = new System.Drawing.Size(82, 24);
            this.normalizedpmnormal_label.TabIndex = 49;
            this.normalizedpmnormal_label.Text = "正常侧";
            // 
            // normalizedpmss_textBox
            // 
            this.normalizedpmss_textBox.Enabled = false;
            this.normalizedpmss_textBox.Location = new System.Drawing.Point(541, 266);
            this.normalizedpmss_textBox.Name = "normalizedpmss_textBox";
            this.normalizedpmss_textBox.Size = new System.Drawing.Size(178, 28);
            this.normalizedpmss_textBox.TabIndex = 48;
            this.normalizedpmss_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.normalizedpmss_textBox_KeyPress);
            // 
            // normalizedpmss_label
            // 
            this.normalizedpmss_label.AutoSize = true;
            this.normalizedpmss_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.normalizedpmss_label.Location = new System.Drawing.Point(453, 266);
            this.normalizedpmss_label.Name = "normalizedpmss_label";
            this.normalizedpmss_label.Size = new System.Drawing.Size(82, 24);
            this.normalizedpmss_label.TabIndex = 47;
            this.normalizedpmss_label.Text = "手续侧";
            // 
            // normalizedppf_label
            // 
            this.normalizedppf_label.AutoSize = true;
            this.normalizedppf_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.normalizedppf_label.Location = new System.Drawing.Point(636, 314);
            this.normalizedppf_label.Name = "normalizedppf_label";
            this.normalizedppf_label.Size = new System.Drawing.Size(202, 24);
            this.normalizedppf_label.TabIndex = 56;
            this.normalizedppf_label.Text = "归一后屈曲峰力值";
            // 
            // normalizedppfnormal_ltextBox
            // 
            this.normalizedppfnormal_ltextBox.Location = new System.Drawing.Point(812, 358);
            this.normalizedppfnormal_ltextBox.Name = "normalizedppfnormal_ltextBox";
            this.normalizedppfnormal_ltextBox.Size = new System.Drawing.Size(179, 28);
            this.normalizedppfnormal_ltextBox.TabIndex = 55;
            this.normalizedppfnormal_ltextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.normalizedppfnormal_ltextBox_KeyPress);
            // 
            // normalizedppfnormal_label
            // 
            this.normalizedppfnormal_label.AutoSize = true;
            this.normalizedppfnormal_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.normalizedppfnormal_label.Location = new System.Drawing.Point(724, 358);
            this.normalizedppfnormal_label.Name = "normalizedppfnormal_label";
            this.normalizedppfnormal_label.Size = new System.Drawing.Size(82, 24);
            this.normalizedppfnormal_label.TabIndex = 54;
            this.normalizedppfnormal_label.Text = "正常侧";
            // 
            // normalizedppfss_textBox
            // 
            this.normalizedppfss_textBox.Location = new System.Drawing.Point(540, 358);
            this.normalizedppfss_textBox.Name = "normalizedppfss_textBox";
            this.normalizedppfss_textBox.Size = new System.Drawing.Size(178, 28);
            this.normalizedppfss_textBox.TabIndex = 53;
            this.normalizedppfss_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.normalizedppfss_textBox_KeyPress);
            this.normalizedppfss_textBox.Validated += new System.EventHandler(this.normalizedppfss_textBox_Validated);
            // 
            // normalizedppfss_label
            // 
            this.normalizedppfss_label.AutoSize = true;
            this.normalizedppfss_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.normalizedppfss_label.Location = new System.Drawing.Point(452, 358);
            this.normalizedppfss_label.Name = "normalizedppfss_label";
            this.normalizedppfss_label.Size = new System.Drawing.Size(82, 24);
            this.normalizedppfss_label.TabIndex = 52;
            this.normalizedppfss_label.Text = "手续侧";
            // 
            // directory_textBox
            // 
            this.directory_textBox.Enabled = false;
            this.directory_textBox.Location = new System.Drawing.Point(457, 446);
            this.directory_textBox.Name = "directory_textBox";
            this.directory_textBox.Size = new System.Drawing.Size(349, 28);
            this.directory_textBox.TabIndex = 57;
            // 
            // outexcellname_textBox
            // 
            this.outexcellname_textBox.Location = new System.Drawing.Point(457, 505);
            this.outexcellname_textBox.Name = "outexcellname_textBox";
            this.outexcellname_textBox.Size = new System.Drawing.Size(285, 28);
            this.outexcellname_textBox.TabIndex = 58;
            this.outexcellname_textBox.Leave += new System.EventHandler(this.outexcellname_textBox_Leave);
            // 
            // flex_label
            // 
            this.flex_label.AutoSize = true;
            this.flex_label.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.flex_label.Location = new System.Drawing.Point(748, 505);
            this.flex_label.Name = "flex_label";
            this.flex_label.Size = new System.Drawing.Size(58, 24);
            this.flex_label.TabIndex = 59;
            this.flex_label.Text = ".xls";
            // 
            // derectory_button
            // 
            this.derectory_button.Location = new System.Drawing.Point(812, 443);
            this.derectory_button.Name = "derectory_button";
            this.derectory_button.Size = new System.Drawing.Size(179, 34);
            this.derectory_button.TabIndex = 60;
            this.derectory_button.Text = "选择存储路径";
            this.derectory_button.UseVisualStyleBackColor = true;
            this.derectory_button.Click += new System.EventHandler(this.derectory_button_Click);
            // 
            // outexcellfile_button
            // 
            this.outexcellfile_button.Location = new System.Drawing.Point(812, 501);
            this.outexcellfile_button.Name = "outexcellfile_button";
            this.outexcellfile_button.Size = new System.Drawing.Size(179, 40);
            this.outexcellfile_button.TabIndex = 61;
            this.outexcellfile_button.Text = "输入名称/选择文件";
            this.outexcellfile_button.UseVisualStyleBackColor = true;
            this.outexcellfile_button.Click += new System.EventHandler(this.outexcellfile_button_Click);
            // 
            // six_comboBox
            // 
            this.six_comboBox.FormattingEnabled = true;
            this.six_comboBox.Items.AddRange(new object[] {
            "男",
            "女"});
            this.six_comboBox.Location = new System.Drawing.Point(113, 139);
            this.six_comboBox.Name = "six_comboBox";
            this.six_comboBox.Size = new System.Drawing.Size(222, 26);
            this.six_comboBox.TabIndex = 62;
            this.six_comboBox.Text = "男";
            this.six_comboBox.SelectedIndexChanged += new System.EventHandler(this.six_comboBox_SelectedIndexChanged);
            // 
            // HosBoneClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.ClientSize = new System.Drawing.Size(1004, 604);
            this.Controls.Add(this.six_comboBox);
            this.Controls.Add(this.outexcellfile_button);
            this.Controls.Add(this.derectory_button);
            this.Controls.Add(this.flex_label);
            this.Controls.Add(this.outexcellname_textBox);
            this.Controls.Add(this.directory_textBox);
            this.Controls.Add(this.normalizedppf_label);
            this.Controls.Add(this.normalizedppfnormal_ltextBox);
            this.Controls.Add(this.normalizedppfnormal_label);
            this.Controls.Add(this.normalizedppfss_textBox);
            this.Controls.Add(this.normalizedppfss_label);
            this.Controls.Add(this.normalizedpm_label);
            this.Controls.Add(this.normalizedpmnormal_textBox);
            this.Controls.Add(this.normalizedpmnormal_label);
            this.Controls.Add(this.normalizedpmss_textBox);
            this.Controls.Add(this.normalizedpmss_label);
            this.Controls.Add(this.bucklingpfv_label);
            this.Controls.Add(this.bucklingpfvnormal_textBox);
            this.Controls.Add(this.bucklingpfvnormal_label);
            this.Controls.Add(this.bucklingpfvss_textBox);
            this.Controls.Add(this.bucklingpfvss_label);
            this.Controls.Add(this.straighteningptnormal_textBox);
            this.Controls.Add(this.MSTS_label);
            this.Controls.Add(this.straighteningptss_textBox);
            this.Controls.Add(this.quadricepsfr_label);
            this.Controls.Add(this.straighteningpt_label);
            this.Controls.Add(this.msts_textBox);
            this.Controls.Add(this.straighteningptnormal_label);
            this.Controls.Add(this.quadricepsfr_textBox);
            this.Controls.Add(this.straighteningptss_label);
            this.Controls.Add(this.osteotomylenght_textBox);
            this.Controls.Add(this.osteotomy_label);
            this.Controls.Add(this.dignose_textBox);
            this.Controls.Add(this.diagnosis_label);
            this.Controls.Add(this.weight_textBox);
            this.Controls.Add(this.weight_label);
            this.Controls.Add(this.height_textBox);
            this.Controls.Add(this.height_label);
            this.Controls.Add(this.age_textBox);
            this.Controls.Add(this.age_label);
            this.Controls.Add(this.sex_label);
            this.Controls.Add(this.name_textBox);
            this.Controls.Add(this.name_label);
            this.Controls.Add(this.hosid_textBox);
            this.Controls.Add(this.hosid_label);
            this.Controls.Add(this.bonneRecord_button);
            this.Name = "HosBoneClass";
            this.Load += new System.EventHandler(this.HosBoneClass_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        /// <summary>
        /// 默认目录
        /// </summary>
        private String defaultPath = "D:\\Hos\\";
        private String defaultFile = "D:\\Hos\\test.xls";

        ///< 处理线程
        private BackgroundWorker bw;

        /// <summary>
        /// 初始化目录
        /// </summary>
        private void InitDeirectory()
        {
            ///< 用配置文件替代上面创建配置文件的方法
            string scanPath;
            if ((scanPath = AppConfig.GetValue("excell_directory")).Equals(string.Empty))
            {
                AppConfig.SetValue("excell_directory", defaultPath);
            }
            else
            {
                defaultPath = scanPath;
            }

            this.directory_textBox.Text = defaultPath;
            if (!Directory.Exists(defaultPath))
            {
                Directory.CreateDirectory(defaultPath);
            }
            if (!defaultPath.EndsWith("\\"))
            {
                defaultPath = defaultPath + "\\";
            }

            string scanFile;
            if ((scanFile = AppConfig.GetValue("excell_file")).Equals(string.Empty))
            {
                AppConfig.SetValue("excell_file", defaultFile);
            }
            else
            {
                defaultFile = scanFile;
            }

            ///< 程序目录才行
            //defaultFile = @"D:\MEME\Excells\HealthPro\HealthPro\bin\Debug\plugsins\test2.xls";
            this.outexcellname_textBox.Text = defaultFile;
            this.flex_label.ForeColor = Color.Gray;
            if (defaultFile.Contains("\\") && !File.Exists(defaultFile))
            {
                File.Copy(NPOI_Helper.modelExlPath, defaultFile);
            }

            ///< 更新默认扫描目录
            AppConfig.SetValue("excell_file", defaultFile);
        }

        /// <summary>
        /// 初始化扫描服务
        /// </summary>
        private void InitScanService()
        {
            ///< 录入处理
            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HosBoneClass_Load(object sender, EventArgs e)
        {
            InitDeirectory();
            InitScanService();
        }

        public HosBoneClass()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 数据完整性检查
        /// </summary>
        /// <returns></returns>
        private bool CheckData()
        {
            if (UITool.checkEmpty(hosid_textBox, name_textBox,
                age_textBox, height_textBox, weight_textBox, 
                dignose_textBox, osteotomylenght_textBox, quadricepsfr_textBox, 
                msts_textBox,straighteningptss_textBox, straighteningptnormal_textBox, 
                bucklingpfvss_textBox, bucklingpfvnormal_textBox, normalizedpmss_textBox, 
                normalizedppfss_textBox, normalizedpmnormal_textBox,
                normalizedppfnormal_ltextBox, outexcellname_textBox))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 目录选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void derectory_button_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (null != defaultPath && !defaultPath.Equals(""))
            {
                ///< 设置此次默认目录为上一次选中目录  
                dialog.SelectedPath = defaultPath;
            }
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string foldPath = dialog.SelectedPath;
                defaultPath = foldPath;
                if (!defaultPath.EndsWith("\\"))
                {
                    defaultPath = defaultPath + "\\";
                }
                this.directory_textBox.Text = defaultPath;

                ///< 更新默认扫描目录
                AppConfig.SetValue("excell_directory", defaultPath);
            }
        }

        /// <summary>
        /// 文件选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void outexcellfile_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (null != defaultFile && !defaultFile.Equals(""))
            {
                ///< 设置此次默认目录为上一次选中目录  
                dialog.FileName = defaultFile;
            }
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.outexcellname_textBox.Text = defaultFile = dialog.FileName;
                ///< 选择文件的情况下，后缀灰掉
                this.flex_label.ForeColor = Color.Gray;

                ///< 更新默认扫描文件
                AppConfig.SetValue("excell_file", defaultFile);


                ///< 更新目录
                defaultPath = defaultFile.Substring(0, defaultFile.LastIndexOf("\\") + 1);
                this.directory_textBox.Text = defaultPath;
                ///< 更新默认扫描目录
                AppConfig.SetValue("excell_directory", defaultPath);
            }
        }

        /// <summary>
        /// 文件名输入焦点离开事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void outexcellname_textBox_Leave(object sender, EventArgs e)
        {
            ///< 文件选择的情况下不用处理后缀问题
            if (this.outexcellname_textBox.Text.Contains("\\"))
            {
                return;
            }
            ///< 文件输入的情况下，后缀打开
            this.flex_label.ForeColor = Color.Red;
            ///< 此时判断如果后缀输入了xls提示去掉
            if (this.outexcellname_textBox.Text.Contains(".xls") ||
                this.outexcellname_textBox.Text.Contains(".xlsx"))
            {
                this.outexcellname_textBox.Text = this.outexcellname_textBox.Text.Substring(0, this.outexcellname_textBox.Text.LastIndexOf("."));
            }
            defaultFile = defaultPath + this.outexcellname_textBox.Text;
            ///< 更新默认扫描目录
            AppConfig.SetValue("excell_file", defaultFile + ".xls");
        }

        /// <summary>
        /// 点击录入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bonneRecord_button_Click(object sender, EventArgs e)
        {
            ///< 数据校验
            if (!CheckData())
            {
                MessageBox.Show("信息输入不完整或有误!");
                return;
            }
            ///< 启动录入
            if (null != bw && !bw.IsBusy)
            {
                bw.RunWorkerAsync();
            }
        }

        ///////////////////数据处理///////////////////////

        /// <summary>
        /// 录入处理_进度通知
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            DialogResult dr =  MessageBox.Show("点击确定-继续录入，点击取消-打开文件所在位置！", e.UserState.ToString(), MessageBoxButtons.OKCancel);
            if(dr == DialogResult.Cancel)
            {
                System.Diagnostics.Process.Start(defaultPath);
            }
        }


        /// <summary>
        /// 性别处理
        /// </summary>
        private String sixV = "男";
        private void six_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (0 == this.six_comboBox.SelectedIndex)
            {
                sixV = "男";
            }
            else
            {
                sixV = "女";
            }
        }

        /// <summary>
        /// 录入处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            ///< 组装数据
            List<String> dataList = UITool.packgeData(
                hosid_textBox, name_textBox,
                age_textBox, height_textBox, weight_textBox,
                dignose_textBox, osteotomylenght_textBox, quadricepsfr_textBox,
                msts_textBox, straighteningptss_textBox, straighteningptnormal_textBox,
                bucklingpfvss_textBox, bucklingpfvnormal_textBox, normalizedpmss_textBox,
                normalizedpmnormal_textBox, normalizedppfss_textBox, normalizedppfnormal_ltextBox);
            dataList.Insert(2, sixV);


            ///< 录入操作
            String optExecllFile;
            if (outexcellname_textBox.Text.ToString().Contains("\\"))
            {
                optExecllFile = outexcellname_textBox.Text.ToString();
            }
            else
            {
                optExecllFile = defaultPath + outexcellname_textBox.Text.ToString() + ".xls";
            }
            HssModelBean hs = NPOI_Helper.ReadModel(optExecllFile, 0);
            if (hs.state)
            {
                if (NPOI_Helper.updateData(hs, optExecllFile, dataList))
                {
                    bw.ReportProgress(100, "录入成功!");
                }
                else
                {
                    bw.ReportProgress(0 , "录入失败!");
                }
            }
            else
            {
                bw.ReportProgress(0, hs.message);
            }
        }

        /// <summary>
        /// 根据伸直峰力矩、身高、体重计算归一后伸直力矩
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void normalizedppfss_textBox_Validated(object sender, EventArgs e)
        {
            if (!UITool.checkEmpty(straighteningptss_textBox, height_textBox, weight_textBox))
            {
                double straighteningpt = Double.Parse(this.straighteningptss_textBox.Text);
                double height = Double.Parse(this.height_textBox.Text);
                double weight = Double.Parse(this.weight_textBox.Text);
                double value = straighteningpt / height / weight;
                this.normalizedpmss_textBox.Text = Convert.ToDouble(value).ToString("0.000");

                straighteningpt = Double.Parse(this.straighteningptnormal_textBox.Text);
                height = Double.Parse(this.height_textBox.Text);
                weight = Double.Parse(this.weight_textBox.Text);
                value = straighteningpt / height / weight;
                this.normalizedpmnormal_textBox.Text = Convert.ToDouble(value).ToString("0.000");
            }
        }

        private void age_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void height_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void weight_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void osteotomylenght_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void msts_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void straighteningptss_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void straighteningptnormal_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void bucklingpfvss_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void bucklingpfvnormal_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void normalizedpmss_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void normalizedpmnormal_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void normalizedppfss_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void normalizedppfnormal_ltextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }

        private void hosid_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            UITool.limmitTextBoxNumber(sender, e);
        }
    }
}
