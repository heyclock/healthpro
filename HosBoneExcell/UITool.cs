﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace HosBoneExcell
{
    class UITool
    {
        /// <summary>
        /// 检查控件是否为空
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static bool checkEmpty(params TextBox[] arr)
        {
            if (null == arr || arr.Length < 1)
            {
                return true;
            }

            for (int i = 0; i < arr.Length; ++i)
            {
                if (arr[i].Text.Equals(""))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 检查控件是否为空
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static List<String> packgeData(params TextBox[] arr)
        {
            List<String> dataList = new List<String>();
            if (null == arr || arr.Length < 1)
            {
                return dataList;
            }

            for (int i = 0; i < arr.Length; ++i)
            {
                dataList.Add(arr[i].Text);
            }

            return dataList;
        }

        /// <summary>
        /// 限制只能输入数字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void limmitTextBoxNumber(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x20) e.KeyChar = (char)0;  //禁止空格键  
            if ((e.KeyChar == 0x2D) && (((TextBox)sender).Text.Length == 0)) return;   //处理负数  
            if (e.KeyChar > 0x20)
            {
                try
                {
                    double.Parse(((TextBox)sender).Text + e.KeyChar.ToString());
                }
                catch
                {
                    e.KeyChar = (char)0;   //处理非法字符  
                }
            }
        }
    }
}
