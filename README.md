# HealthPro

#### 介绍
C#-Winform插件模板工程

#### 软件架构
软件架构说明 - 插件加载界面的方式


#### 安装教程

下载压缩包 <a href="https://gitee.com/heyclock/healthpro/blob/master/HealthPro/bin/Release.zip">Release.zip</a>

运行HealthPro.exe即可

#### 使用说明

1. 利用NPOI操作Excel
2. 利用Excel模板进行内容填充
3. 环境VS2017（社区版） - 需要可以自行安装打包插件进行exe打包

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)